export async function getGoods() {
  const response = await fetch("http://localhost:3000/goods.json");
  const data = await response.json();
  return data;
}
