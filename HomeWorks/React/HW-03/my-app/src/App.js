import Router from "./Router/router";
import Header from "./Component/Header/Header";
import { useEffect, useState } from "react";

function App() {
  const [cartList, setCartList] = useState([]);
  const [wishList, setWishList] = useState([]);

  useEffect(() => {
    getDataFromLocalStorage();
  }, []);

  const getDataFromLocalStorage = () => {
    setWishList(
      localStorage.getItem("wishList")
        ? JSON.parse(localStorage.getItem("wishList"))
        : []
    );

    setCartList(
      localStorage.getItem("shopingList")
        ? JSON.parse(localStorage.getItem("shopingList"))
        : []
    );
  };

  return (
    <>
      <Header cartList={cartList} wishList={wishList} />
      <Router
        wishList={wishList}
        cartList={cartList}
        getDataFromLocalStorage={getDataFromLocalStorage}
      />
    </>
  );
}

export default App;
