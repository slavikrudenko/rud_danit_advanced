import "./header.scss";
import PropTypes from "prop-types";
import { Link } from "react-router-dom";

export default function Header({ cartList, wishList }) {
  return (
    <div className="header">
      <Link className="header__link" to="/products/">
        <p>Home</p>
      </Link>
      <Link className="header__link" to="/products/cart">
        <div className="header__item">
          <p>
            Cart &#128722;
            <span>{cartList && "(" + cartList.length + ")"}</span>
          </p>
        </div>
      </Link>
      <Link className="header__link" to="/products/wish-list">
        <div className="header__item">
          <p>
            Wish List ★<span>{wishList && "(" + wishList.length + ")"}</span>
          </p>
        </div>
      </Link>
    </div>
  );
}

Header.propTypes = {
  basket: PropTypes.number,
  selected: PropTypes.number,
};
