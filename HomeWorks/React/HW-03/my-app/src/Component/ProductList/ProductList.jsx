import React, { useState } from "react";
import ProductCard from "../ProductCard/ProductCard.jsx";
import "./productList.scss";
import PropTypes from "prop-types";

export default function ProductList({
  cartList,
  wishList,
  products,
  getDataFromLocalStorage,
}) {
  const [modal, setModal] = useState({
    isOpen: false,
    title: "Confirm the order",
    isBtnClose: true,
    btn: [{ id: 1, text: "Add to cart" }],
  });
  const [productListData, setProductListData] = useState({
    isMainList: true,
    productOnClick: {},
    isInWishList: "",
  });

  const getProductOnClick = (e) => {
    !modal.isOpen &&
      setProductListData({
        ...productListData,
        productOnClick: products
          .filter(
            (product) => product.article === e.target.getAttribute("data-id")
          )
          .shift(),
      });
  };

  const toggleModal = (e) => {
    getProductOnClick(e);
    setModal({ ...modal, isOpen: !modal.isOpen });
  };

  let isInCartList = cartList.includes(productListData.productOnClick.article);
  const productCard = products.map((product) => {
    let isInWishList = wishList.includes(product.article);

    return (
      <li key={product.article} className="product__card card">
        <ProductCard
          toggleModal={toggleModal}
          modal={modal}
          prodItem={product}
          isInWishList={isInWishList}
          isInCartList={isInCartList}
          wishList={wishList}
          cartList={cartList}
          getDataFromLocalStorage={getDataFromLocalStorage}
          pageData={productListData}
        />
      </li>
    );
  });

  return (
    <div className="product-box">
      <ul className="product">{productCard}</ul>
    </div>
  );
}

ProductList.propTypes = {
  objItems: PropTypes.array,
};
