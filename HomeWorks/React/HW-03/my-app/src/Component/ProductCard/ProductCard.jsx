import "./productCard.scss";
import PropTypes from "prop-types";
import Modal from "../Modal/Modal";

export default function ProductCard({
  toggleModal,
  modal,
  prodItem,
  isInWishList,
  isInCartList,
  getDataFromLocalStorage,
  wishList,
  cartList,
  pageData,
}) {
  const setWishList = () => {
    isInWishList
      ? wishList.splice(wishList.indexOf(prodItem.article), 1)
      : wishList.push(prodItem.article);
    localStorage.setItem("wishList", JSON.stringify(wishList));

    getDataFromLocalStorage();
  };

  const setShopingList = () => {
    if (pageData.isMainList && isInCartList) {
      alert("This product is already added to the card");
    } else if (pageData.isMainList && !isInCartList) {
      cartList.push(pageData.productOnClick.article);
    } else if (!pageData.isMainList) {
      cartList.splice(cartList.indexOf(pageData.productOnClick.article), 1);
    }

    localStorage.setItem("shopingList", JSON.stringify(cartList));
    toggleModal();
    getDataFromLocalStorage();
  };

  return (
    <div className="card__box ">
      {modal.isOpen && (
        <Modal
          modal={modal}
          product={pageData.productOnClick}
          actionWithModal={setShopingList}
          closeModal={toggleModal}
        />
      )}
      <a className="card__link" href={prodItem.Url}>
        <img className="card__img" src={prodItem.img} alt={prodItem.Id} />
      </a>
      <div className="card__title title">
        <h6 className="title__item">{prodItem.Title}</h6>
        <a className="title__link" href={prodItem.Url}>
          <span>{prodItem.Maker}</span>
        </a>
      </div>
      <p className="card__descript">{prodItem.Description}</p>
      <p className="card__color">Color: {prodItem.color}</p>
      <div className="card__info info">
        <p className="info__acticle">Article: {prodItem.article}</p>
        <p className="info__price">Price: {prodItem.price} UAH</p>
      </div>
      {pageData.isMainList && (
        <button
          className="card__btn"
          onClick={toggleModal}
          data-id={prodItem.article}
        >
          Add to cart
        </button>
      )}

      <button onClick={setWishList} className="card__like">
        {(pageData.isMainList || pageData.isWishListPage) &&
          (isInWishList ? "★" : "☆")}
      </button>
      <button
        onClick={toggleModal}
        className="card__like"
        data-id={prodItem.article}
      >
        {pageData.isInCartPage && "X"}
      </button>
    </div>
  );
}

ProductCard.propTypes = {
  objItems: PropTypes.object,
  dataId: PropTypes.string,
  getLengthFromLocalStorage: PropTypes.func,
};
