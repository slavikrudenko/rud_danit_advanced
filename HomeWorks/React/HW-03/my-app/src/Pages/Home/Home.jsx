import { useEffect, useState } from "react";
import ProductList from "../../Component/ProductList/ProductList";
import { getGoods } from "../../Api/api";

export default function Home({ cartList, wishList, getDataFromLocalStorage }) {
  const [products, setProducts] = useState([]);
  const [error, setError] = useState(null);
  const [isLoaded, setIsloaded] = useState(false);

  useEffect(() => {
    getGoods().then(
      (result) => {
        setIsloaded(true);
        setProducts(result);
      },
      (error) => {
        setIsloaded(true);
        setError(error);
      }
    );
  }, []);

  if (error) {
    return <div>Ошибка: {error.message}</div>;
  } else if (!isLoaded) {
    return <div>Загрузка...</div>;
  } else {
    return (
      <ProductList
        cartList={cartList}
        wishList={wishList}
        products={products}
        getDataFromLocalStorage={getDataFromLocalStorage}
      />
    );
  }
}
