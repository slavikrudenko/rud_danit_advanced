import { useEffect, useState } from "react";
import { getGoods } from "../../Api/api";
import ProductCard from "../../Component/ProductCard/ProductCard";
import "./cart.scss";

export default function Cart({ cartList, getDataFromLocalStorage }) {
  const [cart, setCart] = useState([]);
  const [modal, setModal] = useState({
    isOpen: false,
    title: "Are you sure you want to delete this product from the cart?",
    isBtnClose: true,
    btn: [{ id: 1, text: "Yes" }],
  });
  const [cartListData, setCartListData] = useState({
    isInCartPage: true,
    productOnClick: {},
  });

  useEffect(() => {
    getGoods().then((products) => {
      setCart(products.filter((product) => cartList.includes(product.article)));
    });
  }, [cartList]);

  const toggleModal = (e) => {
    !modal.isOpen &&
      setCartListData({
        ...cartListData,
        productOnClick: cart
          .filter(
            (product) => product.article === e.target.getAttribute("data-id")
          )
          .shift(),
      });

    setModal({ ...modal, isOpen: !modal.isOpen });
  };

  const cartListRender = cart.map((productInCart) => {
    return (
      <li key={productInCart.Id} className={"cart__item"}>
        <ProductCard
          toggleModal={toggleModal}
          modal={modal}
          prodItem={productInCart}
          getDataFromLocalStorage={getDataFromLocalStorage}
          pageData={cartListData}
          cartList={cartList}
        />
      </li>
    );
  });

  return (
    <div className="cart">
      <div className="cart__header">
        {cartList.length
          ? "Your cart"
          : "Your cart is empty! Time to start shopping!"}
      </div>
      <ul className="cart__list">{cartListRender}</ul>
    </div>
  );
}
