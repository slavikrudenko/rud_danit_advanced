import { useEffect, useState } from "react";
import { getGoods } from "../../Api/api";
import ProductCard from "../../Component/ProductCard/ProductCard";
import "./wishList.scss";

export default function WishList({ wishList, getDataFromLocalStorage }) {
  const [wish, setWish] = useState([]);
  const [modal, setModal] = useState({
    isOpen: false,
    title: "Are you sure you want to delete this product from the wish list?",
    isBtnClose: true,
    btn: [{ id: 1, text: "Yes" }],
  });
  const [wishListData, setWishListData] = useState({
    isWishListPage: true,
    productOnClick: {},
  });

  useEffect(() => {
    getGoods().then((products) => {
      setWish(products.filter((product) => wishList.includes(product.article)));
    });
  }, [wishList]);

  const toggleModal = (e) => {
    !modal.isOpen &&
      setWishListData({
        ...wishListData,
        productOnClick: wish
          .filter(
            (product) => product.article === e.target.getAttribute("data-id")
          )
          .shift(),
      });

    setModal({ ...modal, isOpen: !modal.isOpen });
  };

  const wishListRender = wish.map((productInWishList) => {
    let isInWishList = wishList.includes(productInWishList.article);

    return (
      <li key={productInWishList.Id} className={"wish__item"}>
        <ProductCard
          toggleModal={toggleModal}
          modal={modal}
          prodItem={productInWishList}
          getDataFromLocalStorage={getDataFromLocalStorage}
          pageData={wishListData}
          wishList={wishList}
          isInWishList={isInWishList}
        />
      </li>
    );
  });

  return (
    <div className="wish">
      <div className="wish__header">
        {wishList.length ? "Your wish list" : "Your wish list is empty! "}
      </div>
      <ul className="wish__list">{wishListRender}</ul>
    </div>
  );
}
