import { Component } from "react";
import React from "react";
import "./modal.scss";
import Button from "../Button/Button";
//import Button from "./Button";

export default class Modal extends Component {
  render() {
    const { modalState, actionClose } = this.props;
    const { modalTitle, modalText, backgroundColor, btn, isBtnClose } =
      modalState;

    return (
      <div
        className="modal"
        onClick={(e) => e.target.className === "modal" && actionClose()}
      >
        <div className="modal__box">
          <div className="modal__box--item">
            <div className="modal__header header">
              <p className="header__title">{modalTitle}</p>
              {isBtnClose && (
                <div className="header__btn btn">
                  <button className="btn__item" onClick={actionClose}>
                    X
                  </button>
                </div>
              )}
            </div>
            <div className="modal__main main">
              <p className="main__text">{modalText}</p>
              <div className="main__btn btn">
                <Button
                  btn={btn}
                  bgColour={backgroundColor}
                  actionClose={actionClose}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
