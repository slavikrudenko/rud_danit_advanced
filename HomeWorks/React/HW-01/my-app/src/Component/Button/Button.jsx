import { Component } from "react";
import "./button.scss";

export default class Button extends Component {
  render() {
    const { btn, bgColour, actionClose } = this.props;
    return (
      <>
        {btn.map((e) => (
          <button
            key={e.id}
            style={{ background: bgColour }}
            className="btn__item"
            onClick={actionClose}
          >
            {e.text}
          </button>
        ))}
      </>
    );
  }
}
