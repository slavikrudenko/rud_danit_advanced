import React from "react";
import Modal from "./Component/Modal/Modal";
import "./app.scss";
import "./App.css";
import { modalWindowDeclarations } from "./objMod";

class App extends React.Component {
  constructor() {
    super();
    App.bind(modalWindowDeclarations);
    this.state = {
      modalToShow: false,
      modalState: {},
    };
  }

  getModalData = (e) => {
    const modalID = e.target.getAttribute("data-modal-id");
    this.modalData = modalWindowDeclarations.find(
      (el) => el.modalId === modalID
    );
    this.setModal();
  };

  setModal = () => {
    this.setState({
      modalToShow: true,
      modalState: {
        modalId: this.modalData.modalId,
        modalTitle: this.modalData.header,
        modalText: this.modalData.text,
        backgroundColor: this.modalData.bgColour,
        btn: this.modalData.btn,
        isBtnClose: this.modalData.delBtn,
      },
    });
  };

  closeModal = () => {
    this.setState({
      modalToShow: false,
    });
  };

  render() {
    const { modalToShow, modalState } = this.state;

    return (
      <>
        <div className="box">
          <button
            data-modal-id="modalID1"
            className="box__btn"
            onClick={this.getModalData}
          >
            Open first modal
          </button>
          <button
            data-modal-id="modalID2"
            className="box__btn"
            onClick={this.getModalData}
          >
            Open second modal
          </button>
        </div>
        {modalToShow && (
          <Modal modalState={modalState} actionClose={this.closeModal} />
        )}
      </>
    );
  }
}
export default App;
