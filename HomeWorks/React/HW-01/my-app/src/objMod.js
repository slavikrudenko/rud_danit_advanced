const modalWindowDeclarations = [
  {
    modalId: "modalID1",
    header: "Do you want to delete this file?",
    text: `Once you delete this file, it won't be possible to undo this action.
  Are you sure you want to delete it?`,
    btn: [
      { text: "ok", id: 0 },
      { text: "Cancel", id: 1 },
    ],
    delBtn: true,
    bgColour: "rgba(13, 0, 255)",
  },
  {
    modalId: "modalID2",
    header: "Delete",
    text: `Are you sure you want to delete?`,
    btn: [
      { text: "Yes", id: 0 },
      { text: "No", id: 1 },
    ],

    delBtn: false,
    bgColour: "rgba(157, 42, 0)",
  },
];

export { modalWindowDeclarations };
