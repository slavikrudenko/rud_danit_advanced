import "./button.scss";
import PropTypes from "prop-types";

export default function Button({ btn, actionWithModal }) {
  return (
    <>
      {btn.map((e) => (
        <button key={e.id} className="btn__item" onClick={actionWithModal}>
          {e.text}
        </button>
      ))}
    </>
  );
}

Button.propTypes = {
  btn: PropTypes.array,
  setLS: PropTypes.func,
};
