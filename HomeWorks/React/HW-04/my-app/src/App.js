import Router from "./Router/router";
import Header from "./Component/Header/Header";
import { setCartListFromLocalStorage } from "./store/actions/cart/cart";
import { setWishListFromLocalStorage } from "./store/actions/wishList/wishList";
import { useDispatch } from "react-redux";

function App() {
  const dispatch = useDispatch();
  dispatch(setCartListFromLocalStorage());
  dispatch(setWishListFromLocalStorage());

  return (
    <>
      <Header />
      <Router />
    </>
  );
}

export default App;
