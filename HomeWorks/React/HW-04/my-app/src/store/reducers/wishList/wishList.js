import {
  getWishListFromLocalStorage,
  addProductToWishList,
  removeProductFromWishList,
} from "../../types";

let wishList = localStorage.getItem("wishList")
  ? JSON.parse(localStorage.getItem("wishList"))
  : [];

const initialState = {
  wishList: [],
};

const reducer = (state = initialState.wishList, action) => {
  switch (action.type) {
    case getWishListFromLocalStorage: {
      return wishList;
    }

    case addProductToWishList: {
      wishList.push(action.payloader);
      localStorage.setItem("wishList", JSON.stringify(wishList));
      return [...state, action.payloader];
    }

    case removeProductFromWishList: {
      const index = wishList.findIndex(
        (product) => product.article === action.payloader.article
      );
      wishList.splice(wishList.indexOf(action.payloader.article), 1);
      localStorage.setItem("wishList", JSON.stringify(wishList));
      return state.filter(
        (product) => product.article !== action.payloader.article
      );
    }

    default:
      return state;
  }
};
export default reducer;
