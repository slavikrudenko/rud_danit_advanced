import {
  addProductToCartList,
  removeProductFromCartList,
  getCartListFromLocalStorage,
  removeAllProductFromCartList,
} from "../../types";

let cartList = localStorage.getItem("shopingList")
  ? JSON.parse(localStorage.getItem("shopingList"))
  : [];

const initialState = {
  cartList: [],
};

const reducer = (state = initialState.cartList, action) => {
  switch (action.type) {
    case addProductToCartList: {
      cartList = [...cartList, action.payloader];
      localStorage.setItem("shopingList", JSON.stringify(cartList));
      return [...state, action.payloader];
    }
    case removeProductFromCartList: {
      const index = cartList.findIndex(
        (product) => product.article === action.payloader.article
      );
      cartList.splice(index, 1);
      localStorage.setItem("shopingList", JSON.stringify(cartList));
      return state.filter(
        (product) => product.article !== action.payloader.article
      );
    }
    case getCartListFromLocalStorage: {
      return cartList;
    }
    case removeAllProductFromCartList: {
      localStorage.setItem("shopingList", JSON.stringify([]));
      return [];
    }

    default:
      return state;
  }
};
export default reducer;
