import {
  addProductToWishList,
  removeProductFromWishList,
  getWishListFromLocalStorage,
} from "../../types";

const addProductsToWish = (product) => {
  return { type: addProductToWishList, payloader: product };
};
const removeProductsFromWish = (product) => {
  return { type: removeProductFromWishList, payloader: product };
};

const setWishListFromLocalStorage = () => {
  return { type: getWishListFromLocalStorage };
};
export {
  setWishListFromLocalStorage,
  addProductsToWish,
  removeProductsFromWish,
};
