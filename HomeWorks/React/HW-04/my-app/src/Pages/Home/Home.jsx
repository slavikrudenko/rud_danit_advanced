import { useEffect } from "react";
import ProductList from "../../Component/ProductList/ProductList";
import { fetchProducts } from "../../store/actions/products/products";
import { useDispatch, useSelector } from "react-redux";

export default function Home() {
  const productsLoad = useSelector((store) => store.productsLoad);
  const { isLoaded, hasError } = productsLoad;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(fetchProducts());
  }, []);

  if (hasError.length) {
    return <div>Ошибка: {hasError.message}</div>;
  } else if (!isLoaded) {
    return <div>Загрузка...</div>;
  } else {
    return <ProductList />;
  }
}
