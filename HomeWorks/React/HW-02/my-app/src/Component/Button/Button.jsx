import { Component } from "react";
import "./button.scss";
import PropTypes from "prop-types";

export default class Button extends Component {
  render() {
    const { btn, setShopingList } = this.props;

    return (
      <>
        {btn.map((e) => (
          <button key={e.id} className="btn__item" onClick={setShopingList}>
            {e.text}
          </button>
        ))}
      </>
    );
  }
}

Button.propTypes = {
  btn: PropTypes.array,
  setLS: PropTypes.func,
};
