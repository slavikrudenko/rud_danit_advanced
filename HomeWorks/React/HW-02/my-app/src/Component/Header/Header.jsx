import { Component } from "react";
import "./header.scss";
import PropTypes from "prop-types";

export default class Header extends Component {
  render() {
    const { basket, selected } = this.props;
    return (
      <>
        <div className="header">
          <div className="header__item">
            <button className="header__card btn"></button>
            <span>{basket !== 0 && basket}</span>
          </div>
          <div className="header__item">
            <button className="header__like btn"> &#9733;</button>
            <span>{selected !== 0 && selected}</span>
          </div>
        </div>
      </>
    );
  }
}
Header.propTypes = {
  basket: PropTypes.number,
  selected: PropTypes.number,
};
