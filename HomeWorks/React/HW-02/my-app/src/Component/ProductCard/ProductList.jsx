import React from "react";
import ProductCard from "../ProductList/ProductCard.jsx";
import "./productList.scss";
import PropTypes from "prop-types";

export default class ProductList extends React.Component {
  render() {
    const { objItems, getLengthFromLocalStorage } = this.props;

    return (
      <ul className="product">
        {objItems.map((e) => (
          <li key={e.article} className="product__card card">
            <ProductCard
              dataId={e.article}
              objItems={objItems}
              getLengthFromLocalStorage={getLengthFromLocalStorage}
            />
          </li>
        ))}
      </ul>
    );
  }
}

ProductList.propTypes = {
  objItems: PropTypes.array,
};
