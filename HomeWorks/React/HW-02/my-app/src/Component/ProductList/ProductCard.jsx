import React from "react";
import "./productCard.scss";
import PropTypes from "prop-types";
import Modal from "../Modal/Modal";

export default class ProductCard extends React.Component {
  constructor(props) {
    super();
    const { objItems, dataId } = props;
    this.prodItem = objItems.find((e) => dataId === e.article);
    this.dataId = dataId;

    this.state = {
      isSelected: false,
      modal: {
        isOpen: false,
        title: "Confirm the order",
        isBtnClose: true,
        btn: [{ id: 1, text: "Add to cart" }],
      },
    };
  }

  componentDidMount() {
    this.setSelectedAfterReload();
  }

  toggleModal = () => {
    this.setState({
      modal: { ...this.state.modal, isOpen: !this.state.modal.isOpen },
    });
  };

  setSelected = (e) => {
    this.setState({
      isSelected: !this.state.isSelected,
    });
    const cardId = e.target.parentNode.getAttribute("data-id");
    let selected = [];
    localStorage.getItem("selected") &&
      (selected = JSON.parse(localStorage.getItem("selected")));

    !this.state.isSelected
      ? selected.push(cardId)
      : selected.indexOf(cardId) !== -1 && selected.splice(0, 1);

    localStorage.setItem("selected", JSON.stringify(selected));
    this.props.getLengthFromLocalStorage();
  };

  setSelectedAfterReload = () => {
    let arrSelected = JSON.parse(localStorage.getItem("selected"));
    arrSelected &&
      arrSelected.find((e) => e === this.dataId) &&
      this.setState({ isSelected: true });
  };

  setShopingList = () => {
    const { article, price } = this.prodItem;
    let arrProducts = [];
    let shopingList = localStorage.getItem("shopingList");
    shopingList && (arrProducts = JSON.parse(shopingList));
    arrProducts.push({
      article: article,
      price: price,
    });

    localStorage.setItem("shopingList", JSON.stringify(arrProducts));
    this.toggleModal();
    this.props.getLengthFromLocalStorage();
  };

  render() {
    return (
      <div className="card__box " data-id={this.dataId}>
        {this.state.modal.isOpen && (
          <Modal
            modal={this.state.modal}
            product={this.prodItem}
            getBasketNum={this.getBasketNum}
            setShopingList={this.setShopingList}
            closeModal={this.toggleModal}
          />
        )}
        <a className="card__link" href={this.prodItem.Url}>
          <img
            className="card__img"
            src={this.prodItem.img}
            alt={this.prodItem.Id}
          />
        </a>
        <div className="card__title title">
          <h6 className="title__item">{this.prodItem.Title}</h6>
          <a className="title__link" href={this.prodItem.Url}>
            <span>{this.prodItem.Maker}</span>
          </a>
        </div>
        <p className="card__descript">{this.prodItem.Description}</p>
        <p className="card__color">Color: {this.prodItem.color}</p>
        <div className="card__info info">
          <p className="info__acticle">Article: {this.prodItem.article}</p>
          <p className="info__price">Price: {this.prodItem.price} UAH</p>
        </div>
        <button className="card__btn" onClick={this.toggleModal}>
          Add to cart
        </button>
        <button onClick={this.setSelected} className="card__like">
          {this.state.isSelected ? "★" : "☆"}
        </button>
      </div>
    );
  }
}

ProductCard.propTypes = {
  objItems: PropTypes.array,
  dataId: PropTypes.string,
  getLengthFromLocalStorage: PropTypes.func,
};
