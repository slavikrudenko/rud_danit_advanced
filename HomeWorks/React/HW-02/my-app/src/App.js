import React from "react";
import "./App.css";
import ProductCard from "./Component/ProductCard/ProductList";
import Header from "./Component/Header/Header";

class App extends React.Component {
  state = {
    error: null,
    isLoaded: false,
    items: [],
    basket: "",
    selected: "",
  };

  componentDidMount() {
    fetch("http://localhost:3000/goods.json")
      .then((res) => res.json())
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result,
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error,
          });
        }
      );
    this.getLengthFromLocalStorage();
  }

  getLengthFromLocalStorage = () => {
    this.setState({
      basket:
        localStorage.getItem("shopingList") &&
        JSON.parse(localStorage.getItem("shopingList")).length,
      selected:
        localStorage.getItem("selected") &&
        JSON.parse(localStorage.getItem("selected")).length,
    });
  };

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Ошибка: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Загрузка...</div>;
    } else {
      return (
        <div className="product-box">
          <Header basket={+this.state.basket} selected={+this.state.selected} />
          <ProductCard
            objItems={items}
            getLengthFromLocalStorage={this.getLengthFromLocalStorage}
          />
        </div>
      );
    }
  }
}

export default App;
