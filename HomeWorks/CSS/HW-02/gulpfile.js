const imagemin = require("imagemin");
const gulp = require("gulp"),
  sass = require("gulp-sass")(require("sass")),
  autoprefixer = require("gulp-autoprefixer"),
  concat = require("gulp-concat"),
  cssmin = require("gulp-minify-css"),
  clean = require("gulp-clean"),
  jsmin = require("gulp-js-minify"),
  cleanCSS = require("gulp-clean-css"),
  uglify = require("gulp-uglify/composer").defaults,
  browserSync = require("browser-sync").create();

function cleaner() {
  return gulp
    .src("./dist/*")
    .pipe(clean())
    .pipe(cleanCSS({ compatibility: "ie8" }));
}
function buildStyles() {
  return (
    gulp
      .src("./src/scss/**/*.scss")
      .pipe(sass().on("error", sass.logError))
      //.pipe(uglify())  not working
      .pipe(
        autoprefixer({
          cascade: false,
        })
      )
      .pipe(concat("styles.min.css"))
      .pipe(cssmin())
      .pipe(gulp.dest("./dist/styles"))
  );
}
function buildJs() {
  return (
    gulp
      .src("./src/js/**/*.js")
      //.pipe(uglify()) not working
      .pipe(concat("scripts.min.js"))
      .pipe(jsmin())
      .pipe(gulp.dest("./dist/js"))
  );
}
function buildImg() {
  return (
    gulp
      .src("./src/img/*")
      //.pipe(imagemin()) not working
      .pipe(gulp.dest("./dist/img"))
  );
}

function watcher() {
  browserSync.init({
    server: {
      baseDir: "./",
    },
  });
  gulp.watch("./index.html").on("change", browserSync.reload);
  gulp.watch("./src/scss/*", buildStyles).on("change", browserSync.reload);
  gulp.watch("./src/js/*", buildJs).on("change", browserSync.reload);
  gulp.watch("./src/img/", buildImg).on("change", browserSync.reload);
}

gulp.task("build", gulp.series(cleaner, buildStyles, buildJs, buildImg));
gulp.task("dev", gulp.series(watcher));
