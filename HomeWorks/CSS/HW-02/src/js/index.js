const menuBtn = document.querySelector(".header-menu-btn"),
  menuBurger = document.querySelector(".header-menu__burger"),
  body = document.querySelector("body");

let menuOpen = false;
menuBtn.addEventListener("click", () => {
  if (!menuOpen) {
    menuBtn.classList.add("open");
    menuBurger.classList.add("open");
    menuOpen = true;
  } else {
    menuBtn.classList.remove("open");
    menuBurger.classList.remove("open");
    menuOpen = false;
  }
});

body.onresize = (e) => {
  if (e.target.innerWidth >= 500) {
    menuBtn.classList.remove("open");
    menuBurger.classList.remove("open");
    menuOpen = false;
  }
};
