class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }
  get name() {
    return this._name;
  }
  set name(value) {
    if (value !== "" && value.length > 1 && isNaN(value)) {
      this._name = value;
    }
  }
  get age() {
    return this._age;
  }
  set age(value) {
    if (value > 16 && value !== "" && !isNaN(value)) {
      this._age = value;
    }
  }
  get salary() {
    return this._salary;
  }
  set salary(value) {
    if (value >= 6500 && value !== "" && !isNaN(value)) {
      this._salary = value;
    }
  }
}
class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }
  get salary() {
    return (this._salary = value * 3);
  }
  set salary(value) {
    this._salary = value;
  }
  get lang() {
    return this._lang;
  }
  set lang(value) {
    if (value !== "" && isNaN(value)) {
      this._lang = value;
    }
  }
}

const programmer = new Programmer("Viacheslav", 25, 6400, "Italian, Spanish");
console.log(programmer);

const programmer2 = new Programmer(
  "Oleg",
  55,
  56500,
  "Polish, Italian, Spanish"
);
console.log(programmer2);

const programmer3 = new Programmer("Vlad", 45, 26500, "English");
console.log(programmer3);

const programmer4 = new Programmer(
  "Roman",
  35,
  16500,
  "English, Polish, Italian, Spanish"
);
console.log(programmer4);

const programmer5 = new Programmer("Vlad", 45, 26500, "English");
console.log(programmer5);
