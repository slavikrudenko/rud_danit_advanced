const div = document.createElement('div')
div.classList.add('box')
document.body.append(div)

const spiner = document.querySelector('.lds-facebook')

class Card {
  renderElement (
    tag,
    classList,
    parent,
    text = '',
    id = '',
    position = 'append'
  ) {
    const element = document.createElement(tag)
    position === 'prepend' ? parent.prepend(element) : parent.append(element)
    if (id !== '') {
      element.setAttribute('data-id', id)
    }
    element.classList.add(classList)
    element.innerHTML = text
    return element
  }

  renderBox = id =>
    (this.box = this.renderElement(`div`, 'box-item', div, '', id))

  renderBoxHeader = () =>
    (this.boxHeader = this.renderElement(`div`, 'box-item-header', this.box))

  renderTitle = () =>
    (this.boxTitle = this.renderElement(
      'h6',
      'box-item_title',
      this.boxHeader,
      this.title
    ))

  renderContent = id =>
    (this.boxContent = this.renderElement(
      'div',
      'box-item_content',
      this.box,
      '',
      id
    ))

  renderContentItem = () =>
    (this.boxContentItem = this.renderElement(
      'p',
      'box-item_content-item',
      this.boxContent,
      this.text
    ))

  renderBoxUser = () =>
    (this.boxUser = this.renderElement(`div`, 'box-item_name', this.box))

  renderUser = () =>
    (this.boxUserName = this.renderElement(
      'p',
      'box-item_name-user',
      this.boxUser,
      this.name
    ))

  renderUserName = () =>
    (this.boxUserName = this.renderElement(
      'p',
      'box-item_name-username',
      this.boxUser,
      `<a href="mailto:${this.email}">@${this.userName}</a>`
    ))

  renderBtnDel (id) {
    this.divBtn = this.renderElement('div', 'box-item_btn', this.boxHeader)
    this.btnDel = this.renderElement(
      'button',
      'box-item_btn-item',
      this.divBtn,
      'X',
      id
    )
    return this.btnDel
  }

  getBtnDel () {
    this.btnDel.addEventListener('click', e => {
      e.preventDefault()
      const btnId = e.target.getAttribute('data-id')
      const divId = document.querySelector(`div[data-id='${btnId}']`)
      fetch(`https://ajax.test-danit.com/api/json/posts/${btnId}`, {
        method: 'DELETE'
      }).then(response => {
        if (response.ok) {
          console.log('post successfully deleted')
          divId.remove()
        } else {
          console.log('Error, try again.')
        }
      })
    })
  }

  renderBtnChange (id) {
    this.divBtn = this.renderElement('div', 'box-item_btn', this.boxHeader)
    this.btnChange = this.renderElement(
      'button',
      'box-item_btn-change',
      this.divBtn
    )
    this.btnChange.setAttribute('data-id', id)
    return this.btnChange
  }

  getBtnChange () {
    this.btnChange.addEventListener('click', e => {
      e.preventDefault()
      const btnId = e.target.getAttribute('data-id')
      const divContentId = document.querySelector(
        `div[data-id='${btnId}'] div[data-id='${btnId}']`
      )

      const contentId = divContentId.querySelector(`p`)
      const changedInput = this.renderElement(
        'textarea',
        'add-content-change',
        divContentId
      )
      changedInput.value = contentId.innerHTML
      contentId.remove()
      const putbtn = this.renderElement(
        'button',
        'add-content-change-btn',
        divContentId,
        'Push changes'
      )
      putbtn.addEventListener('click', () => {
        fetch(`https://ajax.test-danit.com/api/json/posts/${btnId}`, {
          method: 'PUT',
          body: JSON.stringify({
            body: changedInput.value
          })
        })
          .then(response => response.json())
          .then(data => {
            const id = data.id
            this.text = data.body
            this.boxContentItem = this.renderElement(
              'p',
              'box-item_content-item',
              divContentId,
              this.text
            )
            putbtn.remove()
            changedInput.remove()
          })
      })
    })
  }

  renderFromServer (id) {
    this.renderBox(id)
    this.renderBoxHeader()
    this.renderBtnDel(id)
    this.renderTitle()
    this.renderContent(id)
    this.renderContentItem(id)
    this.renderBoxUser()
    this.renderUser()
    this.renderUserName()
    this.getBtnDel()
    this.renderBtnChange(id)
    this.getBtnChange()
  }

  renderAddBtn () {
    this.boxNewContent = this.renderElement(
      'div',
      'add-content',
      document.body,
      '',
      '',
      'prepend'
    )

    this.boxNewContentBtn = this.renderElement(
      'button',
      'add-content-btn',
      this.boxNewContent,
      'Add post'
    )
    return this.boxNewContentBtn
  }

  renderAddInput () {
    this.boxNewContentBtn.addEventListener('click', e => {
      const isInputOpen = document.querySelector('.add-content-input')
      if (!isInputOpen) {
        e.preventDefault()
        this.nputBox = this.renderElement(
          'div',
          'add-content-input',
          this.boxNewContent
        )
        this.newTitleContent = this.renderElement(
          'input',
          'add-content-input-title',
          this.nputBox
        )
        this.newBodyContent = this.renderElement(
          'textarea',
          'add-content-input-body',
          this.nputBox
        )
      } else {
        fetch(`https://ajax.test-danit.com/api/json/posts`, {
          method: 'POST',
          body: JSON.stringify({
            title: this.newTitleContent.value,
            body: this.newBodyContent.value,
            userId: 1
          })
        })
          .then(response => response.json())
          .then(data => {
            console.log('Post successfully added')
            this.title = data.title
            this.text = data.body
            this.box = this.renderElement(
              `div`,
              'box-item',
              div,
              '',
              data.id,
              'prepend'
            )
            this.renderBoxHeader()
            this.renderBtnDel(data.id)
            this.renderTitle()
            this.renderContent(data.id)
            this.renderContentItem()
            this.renderBoxUser()
            this.renderUser()
            this.renderUserName()
            this.getBtnDel()
            this.renderBtnChange(data.id)
            this.getBtnChange()
            this.nputBox.remove()
          })
      }
    })
  }

  renderNewContent () {
    this.renderAddBtn()
    this.renderAddInput()
  }
}

class Posts extends Card {
  constructor (name, userName, email, title, text) {
    super()
    this.name = name
    this.userName = userName
    this.email = email
    this.title = title
    this.text = text
    this.userId = this.userId
  }

  getData = url =>
    fetch(url, {
      method: 'GET'
    }).then(response => response.json())

  renderFromServerContent () {
    Promise.all([
      this.getData('https://ajax.test-danit.com/api/json/users'),
      this.getData('https://ajax.test-danit.com/api/json/posts')
    ]).then(data => {
      const users = data[0]
      const posts = data[1]
      posts.reverse()
      users.reverse()
      users.map(user => {
        const userPosts = posts.filter(post => post.userId === user.id)
        userPosts.map(post => {
          this.name = user.name
          this.userName = user.username
          this.email = user.email
          this.title = post.title
          this.text = post.body
          this.id = post.id
          spiner.remove()
          this.renderFromServer(this.id)
        })
      })
    })
  }
}

const post = new Posts()

post.renderNewContent()
post.renderFromServerContent()
