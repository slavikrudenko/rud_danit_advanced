const urlFilm = `https://ajax.test-danit.com/api/swapi/films`
const divList = document.querySelector('.list')

const getInfo = url => {
  return fetch(url), {}.then(response => response.json())
}

const filmItemInfo = ['episodeId', 'name', 'openingCrawl', 'person']

class CreateTag {
  renderElement (tag, text) {
    const element = document.createElement(tag)
    if (text) {
      element.innerHTML = text
    }
    return element
  }

  createList () {
    this.list = this.renderElement('ul')
    this.list.setAttribute('data-id', this.id)
    divList.append(this.list)
    return this.list
  }

  createListItem (text, attr) {
    this.listItem = this.renderElement('li', `${text}`)
    this.listItem.setAttribute('data-type', attr)
    this.list.append(this.listItem)

    return this.list
  }
}

class FilmInfo extends CreateTag {
  constructor (id, episodeId, name, openingCrawl, characters = 'Loading...') {
    super()
    this.id = id
    this.episodeId = episodeId
    this.name = name
    this.openingCrawl = openingCrawl
    this.characters = characters
  }

  getEpisodesInfo () {
    this.info = getInfo(urlFilm).then(data => {
      data.map(e => {
        this.id = e.id
        this.episodeId = e.episodeId
        this.name = e.name
        this.openingCrawl = e.openingCrawl
        this.getCharacterPercons(e)
        this.render()
      })
    })
  }

  getListElement () {
    let ul = divList.querySelectorAll(`ul`)
    ul.forEach(e => {
      this.li = e.querySelector(`li[data-type='characters']`)
    })
    return this.li
  }

  getSpinner () {
    this.getListElement().classList.add('lds-hourglass')
  }

  getCharacterPercons (data) {
    this.persInfo = data.characters.map(character => {
      return getInfo(character).then(persons => persons.name)
    })

    Promise.all(this.persInfo).then(values => {
      this.characters = values.join(', ')

      //Not Working
      // this.getListElement().classList.remove('lds-hourglass')
      // this.getListElement().innerHTML = this.characters

      let ul = divList.querySelectorAll(`ul`)
      ul.forEach(e => {
        this.li = e.querySelector(`li[data-type='characters']`)
        this.li.classList.remove('lds-hourglass')
        this.li.innerHTML = this.characters
      })
    })
  }

  render () {
    this.createList()
    this.createListItem(this.episodeId, 'episodeId')
    this.createListItem(this.name, 'name')
    this.createListItem(this.characters, 'characters')
    this.createListItem(this.openingCrawl, 'openingCrawl')
    this.getSpinner()
  }
}

const starWars = new FilmInfo()
starWars.getEpisodesInfo()
