const books = [
  {
    author: "Скотт Бэккер",
    name: "Тьма, что приходит прежде",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Воин-пророк",
  },
  {
    name: "Тысячекратная мысль",
    price: 70,
  },
  {
    author: "Скотт Бэккер",
    name: "Нечестивый Консульт",
    price: 70,
  },
  {
    author: "Дарья Донцова",
    name: "Детектив на диете",
    price: 40,
  },
  {
    author: "Дарья Донцова",
    name: "Дед Снегур и Морозочка",
  },
];

const key = ["author", "name", "price"];

const root = document.getElementById("root");

function itemList(arr) {
  const ul = document.createElement("ul");
  root.append(ul);
  arr.forEach((e) => {
    try {
      const notIncluded = key.find((el) => !Object.keys(e).includes(el));
      if (notIncluded) {
        throw new SyntaxError(
          `Ошибка, свойства ${notIncluded} нету в обьекте # ${
            arr.indexOf(e) + 1
          } `
        );
      } else {
        const newArr = Object.values(e);
        const li = document.createElement("li");
        ul.append(li);
        li.innerHTML = newArr;
      }
    } catch (e) {
      console.log(e.message);
    }
  });
}

itemList(books);
