const ipBtn = document.querySelector(".ip-btn");
const div = document.querySelector(".ip");

class UserPosition {
  constructor(continent, country, regionName, city) {
    this.continent = continent;
    this.country = country;
    this.regionName = regionName;
    this.city = city;
  }
  render() {
    const result = document.createElement("div");
    div.append(result);
    result.innerHTML = `
        <h6>Your location:</h6>
        <p>Continent: ${this.continent}</p>
        <p>Country: ${this.country}</p>
        <p>Region: ${this.regionName}</p>
        <p>City: ${this.city}</p>`;
  }
}

async function serverInfo(url) {
  const data = await fetch(url);
  return data.json();
}

async function getUserInfo() {
  const userIp = await serverInfo("https://api.ipify.org/?format=json");
  const query = userIp.ip;

  const userInfo = await serverInfo(
    `http://ip-api.com/json/${query}?fields=continent,country,region,regionName,city,district,zip`
  );

  const userPosition = new UserPosition(
    userInfo.continent,
    userInfo.country,
    userInfo.regionName,
    userInfo.city
  );
  userPosition.render();
}

ipBtn.addEventListener("click", (e) => {
  e.preventDefault();
  getUserInfo();
});
